﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TransTech.Weave.Module;

public class UIController : MonoBehaviour {

    public RectTransform m_ScrollContainer;
    public GameObject m_ButtonPrefab;

    public RectTransform m_StoryEditPanel;


    private void Awake()
    {
        m_StoryEditPanel.GetComponent<StoryEditingPanel>().SetUIController(this);

        var layoutGorup = m_ScrollContainer.GetComponent<VerticalLayoutGroup>();
        RectTransform prefabRect = m_ButtonPrefab.GetComponent<RectTransform>();
        
        var buttonCount = 30;
        var spacing = (4f / 1080f) * Screen.height;
        
        
        var buttonRatio = prefabRect.rect.width / prefabRect.rect.height;
        
        var scrollWidth = m_ScrollContainer.rect.width;
        var buttonWidth = scrollWidth - (spacing * 2);
        var buttonHeight = buttonWidth / buttonRatio;
        var scrollHeight = buttonCount * buttonHeight + (buttonCount + 1) * spacing;
        m_ScrollContainer.offsetMin = new Vector2(m_ScrollContainer.offsetMin.x, -scrollHeight / 2);
        m_ScrollContainer.offsetMax = new Vector2(m_ScrollContainer.offsetMax.x, scrollHeight / 2);
        
        for (int i = 0; i < buttonCount; i++)
        {
            var button = Instantiate<GameObject>(m_ButtonPrefab);
            button.transform.SetParent(m_ScrollContainer, false);

            var buttonRectTransform = button.GetComponent<RectTransform>();

            var x = (-buttonWidth / 2) - spacing;
            var y = -m_ScrollContainer.rect.height / 2 + buttonHeight * i + spacing * (i + 1);
            buttonRectTransform.offsetMin = new Vector2(x, y);

            x = (buttonWidth / 2) + spacing;
            y = buttonRectTransform.offsetMin.y + buttonHeight;
            buttonRectTransform.offsetMax = new Vector2(x, y);
        }
    }

    public void SetEditingModule(NodeModule module)
    {
        if (module is Story)
        {
            m_StoryEditPanel.GetComponent<StoryEditingPanel>().SetStoryModule((Story)module);
            m_StoryEditPanel.GetComponent<StoryEditingPanel>().AnimateOn();
        }
        else if (module is Keyword)
        {
            // TODO : Keyword editor
        }
    }
} 
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransTech.Weave.Module;
using UnityEngine;

namespace TransTech.Weave.Unity
{
    public abstract class NodeCanvas : MonoBehaviour
    {
        public abstract Type ModuleType { get; }

        public abstract bool PopulateUI(NodeModule node);
    }
}

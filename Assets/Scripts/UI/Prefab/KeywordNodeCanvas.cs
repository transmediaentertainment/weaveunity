﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransTech.Weave.Module;
using UnityEngine.UI;
using UnityEngine;

namespace TransTech.Weave.Unity
{
    public class KeywordNodeCanvas : NodeCanvas
    {
        public override Type ModuleType { get { return typeof(Keyword); } }

        public Text m_Description;

        public override bool PopulateUI(Module.NodeModule node)
        {
            if (!(node is Keyword))
            {
                Debug.LogError("Incorrect NodeModule type passed to KeywordNodeCanvas PopulateUI");
                return false;
            }

            var keyword = (Keyword)node;
            var str = "";
            for (int i = 0; i < keyword.KeywordCount; i++)
            {
                str += keyword.GetKeywordString(keyword.GetKeywordId(i)) + "\n";
            }
            m_Description.text = str;
            return true;
        }
    }
}

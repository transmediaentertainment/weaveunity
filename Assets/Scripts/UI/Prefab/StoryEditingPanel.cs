﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TransTech.Weave.Module;
using System;

public class StoryEditingPanel : MonoBehaviour {

    public InputField m_StoryTimeField;
    public InputField m_NarrationTimeField;
    public InputField m_TitleField;
    public InputField m_SummaryField;
    public InputField m_StoryField;
    public Button m_CloseButton;

    private Story m_Story;
    private UIController m_UIController;

    private bool m_IsOnScreen = false;
    private bool m_IsAnimating = false;

    void Awake()
    {
        // Set to off screen pos
        var t = this.GetComponent<RectTransform>();
        var min = t.anchorMin;
        min.x = 1f;
        t.anchorMin = min;
        var max = t.anchorMax;
        max.x = 1.658f;
        t.anchorMax = max;
        m_IsOnScreen = false;

        AnimateOn();

        m_CloseButton.onClick.AddListener(() =>
        {
            this.AnimateOff();
        });

        m_StoryTimeField.onEndEdit.AddListener((value) =>
        {
            if (m_Story != null)
            {
                DateTime result;
                if (DateTime.TryParse(value, out result))
                {
                    m_Story.StoryTime = result;
                }
                // Update the text
                m_StoryTimeField.text = m_Story.StoryTime.ToString();
            }
        });

        m_NarrationTimeField.onEndEdit.AddListener((value) =>
        {
            if (m_Story != null)
            {
                DateTime result;
                if (DateTime.TryParse(value, out result))
                {
                    m_Story.NarrationTime = result;
                }
                // Update the Text
                m_NarrationTimeField.text = m_Story.NarrationTime.ToString();
            }
        });

        m_TitleField.onValueChange.AddListener((value) =>
        {
            if (m_Story != null)
            {
                m_Story.Title = value;
            }
        });

        m_SummaryField.onValueChange.AddListener((value) =>
        {
            if (m_Story != null)
            {
                m_Story.Summary = value;
            }
        });

        m_StoryField.onValueChange.AddListener((value) =>
        {
            if (m_Story != null)
            {
                m_Story.Text = value;
            }
        });
    }

    public void SetUIController(UIController uiController)
    {
        m_UIController = uiController;
    }

    public void SetStoryModule(Story story)
    {
        m_Story = story;

        m_StoryTimeField.text = story.StoryTime.ToString();
        m_NarrationTimeField.text = story.NarrationTime.ToString();
        m_TitleField.text = story.Title;
        m_SummaryField.text = story.Summary;
        m_StoryField.text = story.Text;
    }

    public void AnimateOn()
    {
        if (m_IsAnimating || m_IsOnScreen)
        {
            return;
        }
        m_IsAnimating = true;
        LeanTween.value(gameObject, (float pos) =>
        {
            Debug.Log("On Update On : " + pos);
            var t = this.GetComponent<RectTransform>();
            var min = t.anchorMin;
            min.x = pos;
            t.anchorMin = min;
            var max = t.anchorMax;
            max.x = pos + 0.658f;
            t.anchorMax = max;
        }, 1f, 0.342f, 0.5f).setEase(LeanTweenType.easeOutSine).setOnComplete(() =>
        {
            m_IsAnimating = false;
            m_IsOnScreen = true;
        });
    }

    public void AnimateOff()
    {
        if (m_IsAnimating || !m_IsOnScreen)
        {
            return;
        }
        m_IsAnimating = true;
        LeanTween.value(gameObject, (float pos) =>
        {
            Debug.Log("On Update Off : " + pos);
            var t = this.GetComponent<RectTransform>();
            var min = t.anchorMin;
            min.x = pos;
            t.anchorMin = min;
            var max = t.anchorMax;
            max.x = pos + 0.658f;
            t.anchorMax = max;
        }, 0.342f, 1f, 0.5f).setEase(LeanTweenType.easeOutSine).setOnComplete(() =>
        {
            m_IsAnimating = false;
            m_IsOnScreen = false;
        });
    }
}

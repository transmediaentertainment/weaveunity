﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TransTech.Weave.Module;

namespace TransTech.Weave.Unity
{
    public class StoryNodeCanvas : NodeCanvas
    {
        public override Type ModuleType { get { return typeof(Story); } }
        public Text m_Title;
        public Text m_Summary;
        public Text m_Description;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        public override bool PopulateUI(NodeModule node)
        {
            if (!(node is Story))
            {
                Debug.LogError("Incorrect NodeModule type passed to StoryNodeCanvas PopulateUI");
                return false;
            }

            var story = (Story)node;
            m_Title.text = story.Title;
            m_Summary.text = story.Summary;
            m_Description.text = story.Text;
            return true;
        }
    }
}
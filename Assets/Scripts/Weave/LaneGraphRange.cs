﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransTech.Weave
{
    public abstract class LaneGraphRange<T>
    {
        protected List<T> m_Lanes = new List<T>();
        public abstract bool AddLane(T newLane);
        public int LaneCount { get { return m_Lanes.Count; } }
        public T GetLane(int index)
        {
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException("Index was below zero");
            }

            if (index >= m_Lanes.Count)
            {
                throw new ArgumentOutOfRangeException("Index was too large for Lane Collection");
            }

            return m_Lanes[index];
        }
        public int GetLanePosition(T lane)
        {
            return m_Lanes.IndexOf(lane);
        }
    }
}

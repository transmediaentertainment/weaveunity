﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransTech.Weave
{
    public class KeywordAxis : LaneAxis<int>
    {
        public KeywordAxis()
        {
            m_GraphRange = new KeywordGraphRange();
        }

        public KeywordGraphRange GetRange()
        {
            return (KeywordGraphRange)m_GraphRange;
        }

        
    }
}

﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace TransTech.Weave
{
	public class Graph
	{
		private Dictionary<int,string> m_Keywords;
		private int m_LastId;

		private List<Node> m_Nodes;

        public Axis XAxis { get; private set; }
        public Axis YAxis { get; private set; }

		public Graph() {
			m_Keywords = new Dictionary<int, string>();
			// Initialise m_LastId to -1 so that when we increment below, the first ID becomes 0
			m_LastId = -1;	
			m_Nodes = new List<Node>();
            XAxis = new StoryTimeAxis();
            XAxis.AxisDirection = AxisDirection.X;
            ((LinearAxis)XAxis).SetRangeEnd(100f, new DateTime(1100, 1, 1, 1, 1, 1));
            ((LinearAxis)XAxis).SetRangeStart(0f, new DateTime(1, 1, 1, 1, 1, 1));
            //YAxis = new NarrativeTimeAxis();
           // ((LinearAxis)YAxis).SetRangeEnd(100f, new DateTime(2020, 1, 1, 1, 1, 1));
           // ((LinearAxis)YAxis).SetRangeStart(0f, new DateTime(1900, 1, 1, 1, 1, 1));
            YAxis = new KeywordAxis();
            YAxis.AxisDirection = AxisDirection.Y;
            ((KeywordAxis)YAxis).AddLane(0);
            ((KeywordAxis)YAxis).AddLane(1);
            ((KeywordAxis)YAxis).AddLane(2);
            ((KeywordAxis)YAxis).AddLane(3);
		}

		#region Keywords
		public int FindId( string keyword ) {
			if ( !m_Keywords.ContainsValue(keyword) ) {
				return -1;
			}
			var first = m_Keywords.First((kvp)=>{
				if (kvp.Value==keyword) {
					return true;
				}
				return false;
			});
			return first.Key;
		}

		public string FindKeyword( int id ) {
			if ( m_Keywords.ContainsKey(id) ) {
				return m_Keywords[id];
			}
			return null;
		}

		public KeyValuePair<int,string>[] GetKeywords() {
			return m_Keywords.ToArray();
		}

		public int AddNewKeyword( string keyword ) {
			if ( m_Keywords.ContainsValue(keyword) ) {
				return -1;
			}
			m_LastId++;
			m_Keywords.Add(m_LastId,keyword);
			return m_LastId;
		}

		public bool RemoveKeyword( string keyword ) {
			var id = FindId(keyword);
			if ( id == -1 ) {
				return false;
			}
			m_Keywords.Remove(id);
			return true;
		}
		#endregion

		#region Nodes

		public Node CreateNode() { 
			Node newNode = new Node();
			m_Nodes.Add(newNode);
			return newNode;
		}

		#endregion

	}
}

using System.Collections.Generic;
using TransTech.System.Debug;

namespace TransTech.Weave.Module
{
	public class Keyword : NodeModule
	{
		private List<int> m_KeywordIds;

		private Graph m_Graph;

		public Keyword( Graph graph ) {
			if ( graph == null ) {
				TTDebug.LogError("Keyword: constructor graph can not be null");
			}
			m_Graph = graph;
			m_KeywordIds = new List<int>();
		}

		public bool AddKeywordId(int id) {
			if ( m_Graph.FindKeyword(id) == null ) {
				TTDebug.LogWarning("Keyword: AddKeywordId: id not found");
				return false;
			}
			if ( m_KeywordIds.Contains(id) ) {
				TTDebug.LogWarning("Keyword: AddKeywordId: id already in list");
				return false;
			}
			// Good to go!
			m_KeywordIds.Add(id);
			return true;
		}

		public bool RemoveKeywordId(int id) {
			if ( !m_KeywordIds.Contains(id) ) {
				TTDebug.LogWarning("Keyword: RemoveKeywordId: id not found");
				return false;
			}
			// Good to go!
			m_KeywordIds.Remove(id);
			return true;
		}

        public int KeywordCount { get { return m_KeywordIds.Count; } }
        
        public int GetKeywordId(int index)
        {
            return m_KeywordIds[index];
        }

        public string GetKeywordString(int id)
        {
            return m_Graph.FindKeyword(id);
        }

        public override bool GetPositionsForLaneAxis<T>(GraphAxes axis, LaneGraphRange<T> range, out Dictionary<string, float> positions)
        {
            if (typeof(T) != typeof(int))
            {
                TTDebug.LogError("Keyword : GetPositionsForLaneAxis : Attempting to get Keyword Axis positions with invalid range type");
                positions = null;
                return false;
            }

            positions = new Dictionary<string, float>();
            var laneHeight = 5f;
            for (int i = 0; i < m_KeywordIds.Count; i++)
            {
                positions.Add(m_Graph.FindKeyword(m_KeywordIds[i]), range.GetLanePosition((T)(object)m_KeywordIds[i]) * laneHeight);
            }
            return true;
        }
    }
}


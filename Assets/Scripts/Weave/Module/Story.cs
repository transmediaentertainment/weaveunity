﻿using System;
using TransTech.System.Debug;

namespace TransTech.Weave.Module
{
	
	public class Story : NodeModule
	{
		public DateTime StoryTime		{ get; set; }
		public DateTime	NarrationTime 	{ get; set; }
		public string 	Title 			{ get; set; }
		public string 	Summary			{ get; set; }
		public string	Text			{ get; set; }

        public override bool GetPositionForLinearAxis<T>(GraphAxes axis, LinearGraphRange<T> range, out float position)
        {
            switch (axis)
            {
                case GraphAxes.NarrationTime:
                    if(typeof(T) != typeof(DateTime))
                    {
                        TTDebug.LogError("Story : GetPositionForAxis : Attempting to get NarrationTime Axis position with invalid range type.");
                        position = 0;
                        return false;
                    }
                    return GetDateTimePosition((LinearGraphRange<DateTime>)(object)range, this.NarrationTime, out position);
                case GraphAxes.StoryTime:
                    if (typeof(T) != typeof(DateTime))
                    {
                        TTDebug.LogError("Story : GetPositionForAxis : Attempting to get StoryTime Axis position with invalid range type.");
                        position = 0;
                        return false;
                    }
                    return GetDateTimePosition((LinearGraphRange<DateTime>)(object)range, this.StoryTime, out position);
                default:
                    // We don't handle this axis type
                    position = 0;
                    return false;
            }
        }

        private bool GetDateTimePosition(LinearGraphRange<DateTime> range, DateTime myTime, out float position)
        {
            if (myTime < range.StartRange)
            {
                position = range.StartPosition;
                return false;
            }

            if (myTime > range.EndRange)
            {
                position = range.EndPosition;
                return false;
            }

            var rangeTicks = range.EndRange.Ticks - range.StartRange.Ticks;
            var myTimeInRange = myTime.Ticks - range.StartRange.Ticks;
            var percent = ((double)myTimeInRange) / ((double)rangeTicks);

            var positionRange = range.EndPosition - range.StartPosition;
            position = (float)((positionRange * percent) + range.StartPosition);
            return true;
        }
    }

}

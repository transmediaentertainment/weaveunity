﻿using System;
using System.Collections.Generic;
using TransTech.Weave;

namespace TransTech.Weave.Module
{
	public abstract class NodeModule
	{
        public virtual bool GetPositionForLinearAxis<T>(GraphAxes axis, LinearGraphRange<T> range, out float position)
        {
            position = 0;
            return false;
        }
        public virtual bool GetPositionsForLaneAxis<T>(GraphAxes axis, LaneGraphRange<T> range, out Dictionary<string, float> positions)
        {
            positions = null;
            return false;
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransTech.System.Debug;

namespace TransTech.Weave
{
    public abstract class DateTimeAxis : LinearAxis
    {
        private LinearGraphRange<DateTime> m_GraphRange = new LinearGraphRange<DateTime>();

        public override object GetGraphRange()
        {
            return m_GraphRange;
        }

        public override bool SetRangeStart(float position, object value)
        {
            if (!(value is DateTime))
            {
                TTDebug.LogError("StoryTimeAxis : SetRangeStart : Must call SetRangeStart with DateTime type for value");
                return false;
            }

            var timeValue = (DateTime)value;

            if (timeValue > m_GraphRange.EndRange)
            {
                TTDebug.LogError("StoryTimeAxis : SetRangeStart : Can't set range start after range end");
                return false;
            }

            // TODO : Do we need to check position as well?  We could possibly want a backwards timeline?

            m_GraphRange.StartPosition = position;
            m_GraphRange.StartRange = timeValue;
            return true;
        }

        public override bool SetRangeEnd(float position, object value)
        {
            if (!(value is DateTime))
            {
                TTDebug.LogError("StoryTimeAxis : SetRangeEnd : Must call SetRangeEnd with DateTime type for value");
                return false;
            }

            var timeValue = (DateTime)value;

            if (timeValue < m_GraphRange.StartRange)
            {
                TTDebug.LogError("StoryTimeAxis : SetRangeEnd : Can't set range end before range start");
                return false;
            }

            // TODO : Do we need to check position as well?  We could possibly want a backwards timeline?

            m_GraphRange.EndPosition = position;
            m_GraphRange.EndRange = timeValue;
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransTech.Weave
{
    public class LinearGraphRange<T>
    {
        public T StartRange { get; set; }
        public float StartPosition { get; set; }
        public T EndRange { get; set; }
        public float EndPosition { get; set; }
    }
}

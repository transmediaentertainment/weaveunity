using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TransTech.Weave.Module;

namespace TransTech.Weave.Unity
{
	public class GraphController : MonoBehaviour
	{
		private Graph m_Graph;
        private List<NodeController> m_NodeControllers = new List<NodeController>();

		// Use this for initialization
		void Awake ()
		{
			m_Graph = new Graph();

            var ids = new int[4];
            ids[0] = m_Graph.AddNewKeyword("Fred");
            ids[1] = m_Graph.AddNewKeyword("Barney");
            ids[2] = m_Graph.AddNewKeyword("WIIIIIIIIIIIIIILMA");
            ids[3] = m_Graph.AddNewKeyword("Betty");

            for (int i = 0; i < 10; i++)
            {
                var nc = CreateNode();
                var node1 = nc.Node;
                node1.AddModule(new Story
                {
                    StoryTime = new DateTime(1 + i * 100, 6, 21, 12, 0, 0),
                    NarrationTime = new DateTime(UnityEngine.Random.Range(1900, 2015), 6, 21, 12, 0, 0),
                    Title = "Story Event : " + (i + 1),
                    Summary = "This is story event number " + (i + 1),
                    Text = "This is the text for the story event numbered " + (i + 1)
                });
                var kw = new Keyword(m_Graph);
                node1.AddModule(kw);
                var count = UnityEngine.Random.Range(1, ids.Length);
                var loopLimit = 0;
                for (int j = 0; j < count; j++)
                {
                    loopLimit = 0;
                    bool done = false;
                    while(!done && loopLimit < 10) {
                        var rand = UnityEngine.Random.Range(0, ids.Length);
                        done = kw.AddKeywordId(rand);
                        loopLimit++;
                    }
                }
                
                //nc.DisplayModuleInfoUI(typeof(Story));
                //nc.DisplayModuleInfoUI(typeof(Keyword));
                m_NodeControllers.Add(nc);
            }

            UpdateNodePositions();
		}
		
		// Update is called once per frame
		void Update ()
		{

		}

		public NodeController CreateNode() {
            var go = new GameObject("Node");//GameObject.CreatePrimitive(PrimitiveType.Cube);
			var nc = go.AddComponent<NodeController>();
			var node  = m_Graph.CreateNode ();
            nc.SetNode(node);
			return nc;
		}

        public void UpdateNodePositions()
        {
            for (int i = 0; i < m_NodeControllers.Count; i++)
            {
                m_NodeControllers[i].DisplayNodeInGraph(m_Graph.XAxis, m_Graph.YAxis);
                //m_NodeControllers[i].DisplayModuleInfoUI(typeof(Keyword));
            }
        }
	}
}


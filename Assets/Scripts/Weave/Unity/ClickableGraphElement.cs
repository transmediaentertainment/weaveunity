﻿using UnityEngine;
using System.Collections;
using System;

public class ClickableGraphElement : MonoBehaviour 
{
    public static event Action<ClickableGraphElement> ElementClicked;

    public NodeController NodeController { get; set; }

    void OnMouseDown()
    {
        if (ElementClicked != null)
        {
            ElementClicked(this);
        }
    }
}

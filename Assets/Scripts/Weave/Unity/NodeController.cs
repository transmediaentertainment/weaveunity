using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TransTech.Weave;
using TransTech.Weave.Module;
using TransTech.Weave.Unity;

public class NodeController : MonoBehaviour
{
    public Node Node { get; private set; }
    private Dictionary<Type, NodeCanvas> m_UIPrefabs = new Dictionary<Type, NodeCanvas>();
    private Dictionary<Type, NodeCanvas> m_UIInstances = new Dictionary<Type, NodeCanvas>();

    private List<GameObject> m_VisualObjects = new List<GameObject>();
    private NodeCanvas m_NodeCanvas;

    public bool SetNode(Node node)
    {
        if (this.Node != null)
        {
            Debug.LogError("Error setting node.  Node was already populated.");
            return false;
        }

        this.Node = node;
        this.Node.NodeModuleAdded += HandleNodeModuleAdded;
        this.Node.NodeModuleRemoved += HandleNodeModuleRemoved;
        return true;
    }

    public NodeCanvas DisplayModuleInfoUI(Type type, Transform parent)
    {
        NodeCanvas nc = null;
        if (!m_UIInstances.ContainsKey(type))
        {
            var prefab = m_UIPrefabs[type];
            var go = (GameObject)Instantiate(prefab.gameObject, transform.position, transform.rotation);
            if (go != null)
            {
                nc = go.GetComponent<NodeCanvas>();
                m_UIInstances.Add(type, nc);
            }
        }
        else
        {
            var go = m_UIInstances[type].gameObject;
            go.SetActive(true);
            nc = go.GetComponent<NodeCanvas>();
        }

        nc.PopulateUI(this.Node.GetNodeModule(type));

        nc.transform.SetParent(parent, false);
        nc.transform.localPosition = new Vector3(3.0f, 0f);
        m_NodeCanvas = nc;

        return nc;
    }

    public void HideModuleInfoUI()
    {
        foreach (var kvp in m_UIInstances)
        {
            if (kvp.Value.gameObject.activeInHierarchy)
            {
                kvp.Value.gameObject.SetActive(false);
            }
        }
    }

    void HandleNodeModuleRemoved(NodeModule obj)
    {
        m_UIPrefabs.Remove(obj.GetType());
    }

    void HandleNodeModuleAdded(NodeModule obj)
    {
        var prefab = UIPrefabs.Instance.GetPrefab(obj.GetType());
        if (prefab == null)
        {
            Debug.LogError("Couldn't find UI Prefab for type : " + obj.GetType());
            return;
        }
        m_UIPrefabs.Add(obj.GetType(), prefab);
    }

    public void DisplayNodeInGraph(params Axis[] axes)
    {
        if (axes == null)
            return;

        CleanVisualObjects();
        m_VisualObjects.Clear();


        for (int i = 0; i < axes.Length; i++)
        {
            var axis = axes[i];
            if (axis is StoryTimeAxis || axis is NarrativeTimeAxis)
            {
                var story = Node.GetNodeModule<Story>();
                if (story == null)
                {
                    CleanVisualObjects();
                    return;
                }
                float pos;
                if (!story.GetPositionForLinearAxis<DateTime>(GraphAxes.StoryTime, (LinearGraphRange<DateTime>)((StoryTimeAxis)axis).GetGraphRange(), out pos))
                {
                    CleanVisualObjects();
                    return;
                }
                if (m_VisualObjects.Count == 0)
                {
                    AddNewVisualObject();
                }
            }
            if (axis is KeywordAxis)
            {
                var keyword = Node.GetNodeModule<Keyword>();
                if (keyword == null)
                {
                    CleanVisualObjects();
                    return;
                }
                Dictionary<string, float> positions;
                if(!keyword.GetPositionsForLaneAxis<int>(GraphAxes.Keyword, ((KeywordAxis)axis).GetRange(), out positions))
                {
                    CleanVisualObjects();
                    return;
                }
                while (m_VisualObjects.Count < keyword.KeywordCount)
                {
                    AddNewVisualObject();
                }
            }
        }

        for (int i = 0; i < axes.Length; i++)
        {
            var axis = axes[i];
            if (axis is StoryTimeAxis)
            {
                var story = Node.GetNodeModule<Story>();
                float pos;
                if (!story.GetPositionForLinearAxis<DateTime>(GraphAxes.StoryTime, (LinearGraphRange<DateTime>)((StoryTimeAxis)axis).GetGraphRange(), out pos))
                {
                    Debug.LogError("Error : Invalid execution path.");
                    continue;
                }
                SetUniformVisualPositions(axis.AxisDirection, pos);
            }
            else if (axis is NarrativeTimeAxis)
            {
                var story = Node.GetNodeModule<Story>();
                float pos;
                if (!story.GetPositionForLinearAxis<DateTime>(GraphAxes.NarrationTime, (LinearGraphRange<DateTime>)((NarrativeTimeAxis)axis).GetGraphRange(), out pos))
                {
                    Debug.LogError("Error : Invalid execution path.");
                    continue;
                }
                SetUniformVisualPositions(axis.AxisDirection, pos);
            }
            else if (axis is KeywordAxis)
            {
                var keywords = Node.GetNodeModule<Keyword>();
                Dictionary<string, float> positions;
                if(!keywords.GetPositionsForLaneAxis<int>(GraphAxes.Keyword, ((KeywordAxis)axis).GetRange(), out positions))
                {
                    Debug.LogError("Error : Invalid execution path.");
                    continue;
                }
                var laneCount = ((KeywordAxis)axis).GetRange().LaneCount;
                int j = 0;
                foreach(var kvp in positions)
                {
                    SetVisualObjectPosition(axis.AxisDirection, kvp.Value, m_VisualObjects[j].transform);
                    j++;
                }
            }
        }
    }

    private void AddNewVisualObject()
    {
        var go = GameObject.CreatePrimitive(PrimitiveType.Cube);
        var cge = go.AddComponent<ClickableGraphElement>();
        cge.NodeController = this;
        ClickableGraphElement.ElementClicked += HandleElementClicked;
        m_VisualObjects.Add(go);
    }

    void HandleElementClicked(ClickableGraphElement obj)
    {
        if (obj.NodeController != this)
        {
            HideModuleInfoUI();
        }
        else
        {
            DisplayModuleInfoUI(typeof(Keyword), obj.transform);
        }
    }

    private void SetVisualObjectPosition(AxisDirection dir, float val, Transform t)
    {
        var tPos = t.position;
        switch (dir)
        {
            case AxisDirection.X:
                tPos.x = val;
                break;
            case AxisDirection.Y:
                tPos.y = val;
                break;
            case AxisDirection.Z:
                tPos.z = val;
                break;
        }
        t.position = tPos;
    }

    private void SetUniformVisualPositions(AxisDirection dir, float val)
    {
        for (int i = 0; i < m_VisualObjects.Count; i++)
        {
            SetVisualObjectPosition(dir, val, m_VisualObjects[i].transform);
        }
    }

    private void CleanVisualObjects()
    {
        // TODO : Replace this with proper object pooling.
        for (int i = 0; i < m_VisualObjects.Count; i++)
        {
            Destroy(m_VisualObjects[i]);
            m_VisualObjects[i] = null;
        }
    }
}


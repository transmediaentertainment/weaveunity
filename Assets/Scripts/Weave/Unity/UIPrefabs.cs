using UnityEngine;
using System;
using System.Linq;
using TransTech.System.Unity;

namespace TransTech.Weave.Unity
{
	public class UIPrefabs : MonoSingleton<UIPrefabs>
	{
        public NodeCanvas[] m_Prefabs;

        public NodeCanvas GetPrefab(Type type)
        {
            return m_Prefabs.FirstOrDefault<NodeCanvas>((nc) => {
                return nc.ModuleType == type;
            });
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransTech.Weave
{
    public abstract class LinearAxis : Axis
    {
        public abstract bool SetRangeStart(float position, object value);
        public abstract bool SetRangeEnd(float position, object value);
        public abstract object GetGraphRange();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransTech.Weave
{
    public enum AxisDirection
    {
        X,
        Y,
        Z,

        Count
    }
    public abstract class Axis
    {
        public AxisDirection AxisDirection { get; set; }
    }
}

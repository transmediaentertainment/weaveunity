﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransTech.Weave
{
    public class KeywordGraphRange : LaneGraphRange<int>
    {
        public override bool AddLane(int newLane)
        {
            m_Lanes.Add(newLane);
            return true;
        }
    }
}

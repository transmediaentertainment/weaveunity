﻿using System;
using System.Collections.Generic;
using System.Linq;
using TransTech.System.Debug;
using TransTech.Weave.Module;

namespace TransTech.Weave
{
	public class Node
	{
		private List<NodeModule> m_NodeModules;
		public event Action<NodeModule> NodeModuleAdded;
		public event Action<NodeModule> NodeModuleRemoved;

		public Node() {
			m_NodeModules = new List<NodeModule>();
		}

		public bool AddModule( NodeModule module ) { 
			if ( m_NodeModules.FirstOrDefault((nc)=> {
				return nc.GetType() == module.GetType();
			}) != null) {
				TTDebug.LogError("Node: AddModule: module already exists on node");
				return false;
			}
			// Go for it!
			m_NodeModules.Add(module);
			if(NodeModuleAdded != null) {
				NodeModuleAdded(module);
			}
			return true;
		}

		public T GetNodeModule<T>() where T : NodeModule {
			return (T)m_NodeModules.FirstOrDefault((nc)=> {
				return nc.GetType() == typeof(T);
			});
		}

        public NodeModule GetNodeModule(Type type)
        {
            return m_NodeModules.FirstOrDefault((nc) =>
            {
                return nc.GetType() == type;
            });
        }

		public NodeModule[] GetNodeModules() {
			return m_NodeModules.ToArray();
		}

		public bool RemoveModule( NodeModule module ) {
			if ( !m_NodeModules.Contains(module) ) {
				TTDebug.LogError("Node: RemoveModule: module does not exist on node");
				return false;
			}
			// Go for it!
			m_NodeModules.Remove(module);
			if(NodeModuleRemoved != null) {
				NodeModuleRemoved(module);
			}
			return true;
		}
	}
}

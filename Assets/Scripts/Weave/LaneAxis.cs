﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransTech.Weave
{
    public abstract class LaneAxis<T> : Axis
    {
        protected LaneGraphRange<T> m_GraphRange;
        public bool AddLane(T newLane)
        {
            return m_GraphRange.AddLane(newLane);
        }
    }
}

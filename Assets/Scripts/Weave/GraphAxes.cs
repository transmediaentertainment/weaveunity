﻿namespace TransTech.Weave
{
    public enum GraphAxes
    {
        StoryTime,
        NarrationTime,
        Keyword
    }
}

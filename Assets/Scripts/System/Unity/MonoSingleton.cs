﻿using UnityEngine;

namespace TransTech.System.Unity
{
    /// <summary>
    /// A singleton wrapper for MonoBehaviour objects
    /// </summary>
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        /// <summary>
        /// Gets a value indicating whether this <see cref="MonoSingleton`1"/> exists.
        /// </summary>
        /// <value><c>true</c> if exists; otherwise, <c>false</c>.</value>
        public static bool Exists
        {
            get { return m_Instance != null; }
        }

        /// <summary>
        /// The instance (initialised to null).
        /// </summary>
        private static T m_Instance = null;

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static T Instance
        {
            get
            {
                // Instance requiered for the first time, we look for it
                if (m_Instance == null)
                {
                    m_Instance = GameObject.FindObjectOfType(typeof(T)) as T;

                    // Object not found, we create a temporary one
                    if (m_Instance == null)
                    {
                        UnityEngine.Debug.LogWarning("No instance of " + typeof(T).ToString() + ", a temporary one is created.");
                        m_Instance = new GameObject("Temp Instance of " + typeof(T).ToString(), typeof(T)).GetComponent<T>();

                        // Problem during the creation, this should not happen
                        if (m_Instance == null)
                        {
                            UnityEngine.Debug.LogError("Problem during the creation of " + typeof(T).ToString());
                        }
                    }

                    // DontDestroyOnLoad(m_Instance.gameObject);
                    m_Instance.Init();
                }
                return m_Instance;
            }
        }

        // If no other monobehaviour request the instance in an awake function
        // executing before this one, no need to search the object.
        /// <summary>
        /// Awake this instance.
        /// </summary>
        private void Awake()
        {
            if (m_Instance == null)
            {
                m_Instance = this as T;
                m_Instance.Init();
            }
            else
            {
                // Make sure there aren't duplicates in the scene.
                if (m_Instance != this as T)
                {
                    Destroy(gameObject);
                }
            }
        }

        // This function is called when the instance is used the first time
        // Put all the initializations you need here, as you would do in Awake/
        /// <summary>
        /// Init this instance.
        /// </summary>
        public virtual void Init()
        {
        }

        /// <summary>
        /// Destroys the MonoSingleton.
        /// </summary>
        private void OnDestroy()
        {
            m_Instance = null;
        }

        /// <summary>
        /// Make sure the instance isn't referenced anymore when the user quit, just in case.
        /// </summary>
        private void OnApplicationQuit()
        {
            m_Instance = null;
        }
    }
}
namespace TransTech.System.Debug
{
	/// <summary>
	/// TransTech's debug system
	/// </summary>
	public static class TTDebug
	{
		/// <summary>
		/// Performs a standard log.  Use for basic logging information
		/// </summary>
		/// <param name="message">The message to log</param>
		public static void Log(string message)
		{
			UnityEngine.Debug.Log(message);
		}
		
		/// <summary>
		/// Performs a warning log.  Use for logging information about problems
		/// that might arise from certain behaviour/input
		/// </summary>
		/// <param name="message">The message to log</param>
		public static void LogWarning(string message)
		{
			UnityEngine.Debug.LogWarning(message);
		}
		
		/// <summary>
		/// Performs an Error log.  Use for logging when something goes wrong and
		/// requires attention from a developer immediately.
		/// </summary>
		/// <param name="message"></param>
		public static void LogError(string message)
		{
			UnityEngine.Debug.LogError(message);
		}
	}
}
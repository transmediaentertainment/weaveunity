﻿using TransTech.System.Collections.Generic;
using TransTech.System.Unity;

namespace TransTech.FiniteStateMachine
{
    /// <summary>
    /// Finite State Machine class
    /// </summary>
    public class FSM
    {
        /// <summary>
        /// The stack of states
        /// </summary>
        private readonly ListStack<IFSMState> m_StateStack = new ListStack<IFSMState>();

        /// <summary>
        /// Initializes a new instance of the <see cref="TransTech.FiniteStateMachine.FSM"/> class.
        /// </summary>
        public FSM()
        {
            SystemEventCenter.Instance.UpdateEvent += FSMUpdate;
        }

        /*~FSM()
        {
			if(SystemEventCenter.Exists)
            	SystemEventCenter.Instance.UpdateEvent -= FSMUpdate;
        }*/

        private void FSMUpdate(float deltaTime)
        {
            if (m_StateStack.Count > 0)
            {
                var currentState = m_StateStack.Peek();
                currentState.Update(deltaTime);

                for (int i = 0; i < m_StateStack.Count - 2; i++)
                {
                    m_StateStack[i].UnfocusedUpdate(deltaTime);
                }
            }
        }

        /// <summary>
        /// Pops all stacked states, and sets the given state as the new state
        /// </summary>
        /// <param name="newState">State to set as the new state</param>
        public void NewState(IFSMState newState, params object[] objs)
        {
            while (m_StateStack.Count > 0)
            {
                var state = m_StateStack.Pop();
                state.Exit();
            }

            newState.Enter(objs);
            m_StateStack.Push(newState);
        }

        /// <summary>
        /// Pushes the new State onto the stack.  The Current State loses focus.
        /// </summary>
        /// <param name="newState">The new state to push onto the stack</param>
        public void PushState(IFSMState newState, params object[] objs)
        {
            var currentState = m_StateStack.Peek();
            if (currentState != null)
                currentState.LostFocus();

            newState.Enter(objs);
            m_StateStack.Push(newState);
        }

        /// <summary>
        /// Pops the current state from the top of the stack and returns it.  If there
        /// is another state left in the stack, it regains focus.
        /// </summary>
        /// <returns>The state popped from the stack, or NULL if the stack is empty</returns>
        public IFSMState PopState()
        {
            IFSMState returnState = null;
            if (m_StateStack.Count > 0)
            {
                returnState = m_StateStack.Pop();
                returnState.Exit();

                if (m_StateStack.Count > 0)
                {
                    var newState = m_StateStack.Peek();
                    newState.GainedFocus();
                }
            }
            return returnState;
        }

        /// <summary>
        /// Peeks at the current state from the top of the stack.
        /// </summary>
        public IFSMState Peek()
        {
            if (m_StateStack.Count > 0)
                return m_StateStack.Peek();
            return null;
        }
    }
}
﻿namespace TransTech.FiniteStateMachine
{
    public interface IFSMState
    {
        void Enter(params object[] args);

        void Update(float deltaTime);

        void UnfocusedUpdate(float deltaTime);

        void Exit();

        void LostFocus();

        void GainedFocus();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransTech.FiniteStateMachine;
using UnityEngine;

namespace TransTech.Weave.Unity
{
    public class CameraLerpingState : FSMState
    {
        private CameraController m_Controller;
        private ClickableGraphElement m_CurrentTarget;
        private Vector3 m_TargetPos;
        private float m_TotalLerpTime = 1f;

        public event Action LerpCompleted;

        public CameraLerpingState(CameraController controller)
        {
            m_Controller = controller;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            if (args == null || args.Length != 1 || !(args[0] is ClickableGraphElement))
            {
                Debug.LogError("Invalid arguements passing into CameraLerpingState Enter.");
                return;
            }

            m_CurrentTarget = (ClickableGraphElement)args[0];
            m_TargetPos = m_CurrentTarget.transform.position + new Vector3(0, 0, -9);
            LeanTween.move(m_Controller.gameObject, m_TargetPos, m_TotalLerpTime).setEase(LeanTweenType.easeInOutQuint).setOnComplete(() => {
                if (LerpCompleted != null)
                {
                    LerpCompleted();
                }
            });
        }
    }
}

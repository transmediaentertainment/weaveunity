﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransTech.FiniteStateMachine;
using UnityEngine;

namespace TransTech.Weave.Unity
{
    public class CameraPanningState : FSMState
    {
        private CameraController m_Controller;
        private Vector3 m_LastMousePos;

        public CameraPanningState(CameraController controller)
        {
            m_Controller = controller;
            m_LastMousePos = Input.mousePosition;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            var newMousePos = Input.mousePosition;
            var deltaMousePost = m_LastMousePos - newMousePos;
            var pos = m_Controller.transform.position;
            if (Input.GetMouseButton(1))
            {
                // Pan
                pos += deltaMousePost * m_Controller.m_PanRate * Time.deltaTime;
            }

            // Zoom

            pos.z += Input.GetAxis("Mouse ScrollWheel") * m_Controller.m_ZoomRate;

            m_Controller.transform.position = pos;
            m_LastMousePos = newMousePos;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransTech.FiniteStateMachine;

namespace TransTech.Weave.Unity
{
    public class CameraFSM : FSM
    {
        private CameraPanningState m_PanningState;
        private CameraLerpingState m_LerpingState;

        public CameraFSM(CameraController controller)
        {
            m_PanningState = new CameraPanningState(controller);
            m_LerpingState = new CameraLerpingState(controller);
            m_LerpingState.LerpCompleted += HandleLerpCompleted;

            ClickableGraphElement.ElementClicked += HandleElementClicked;

            NewState(m_PanningState);
        }

        void HandleLerpCompleted()
        {
            NewState(m_PanningState);
        }

        void HandleElementClicked(ClickableGraphElement obj)
        {
            if (Peek() != m_LerpingState)
            {
                NewState(m_LerpingState, obj);
            }
        }

    }
}

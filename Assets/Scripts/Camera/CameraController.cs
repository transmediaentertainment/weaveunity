﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

namespace TransTech.Weave.Unity
{
    public class CameraController : MonoBehaviour
    {
        public float m_PanRate = 5f;
        public float m_ZoomRate = 5f;
        
        private CameraFSM m_FSM;


        void Awake()
        {
            m_FSM = new CameraFSM(this);
        }
    }
}
